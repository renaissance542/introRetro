package com.philroy.introretro.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.philroy.introretro.databinding.ListItemBinding
import com.philroy.introretro.model.Bird

class ShibeAdapter: RecyclerView.Adapter<ShibeAdapter.ShibeViewHolder>() {

    private lateinit var birds : List<Bird>

    class ShibeViewHolder(
        private val binding: ListItemBinding
    ): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Bird) {
            binding.imageShibe.loadImage(item.url)
        }

        fun ImageView.loadImage(url: String) {
            Glide.with(context).load(url).into(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShibeViewHolder {
        val binding = ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ShibeViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ShibeViewHolder, position: Int) {
        holder.bind(birds[position])
    }

    override fun getItemCount(): Int {
        return birds.size
    }

    fun loadShibes(shibes: List<Bird>) {
        this.birds = shibes
    }


}