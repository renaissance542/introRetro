package com.philroy.introretro.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.philroy.introretro.databinding.FragmentFirstBinding
import com.philroy.introretro.model.MainRepo
import com.philroy.introretro.model.util.Resource
import com.philroy.introretro.view.adapter.ShibeAdapter
import com.philroy.introretro.viewmodel.MainViewModel
import com.philroy.introretro.viewmodel.factories.ViewModelFactoryMain

class FirstFragment : Fragment() {
    private var _binding : FragmentFirstBinding? = null
    private val binding get() = _binding!!

    private val repo by lazy {
        MainRepo(requireContext())
    }
    private val mainViewModel by viewModels<MainViewModel>() {
        ViewModelFactoryMain(repo)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentFirstBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() = with(binding) {
        rvShibe.layoutManager = GridLayoutManager(context, 2)
        mainViewModel.viewState.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Error -> {}
                is Resource.Loading -> {}
                is Resource.Success -> {
                    rvShibe.adapter = ShibeAdapter().apply { loadShibes(it.data) }
                }
            }

        }
    }
}