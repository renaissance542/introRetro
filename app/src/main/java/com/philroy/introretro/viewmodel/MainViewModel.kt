package com.philroy.introretro.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.philroy.introretro.model.Bird
import com.philroy.introretro.model.MainRepo
import com.philroy.introretro.model.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel(private val repo: MainRepo) : ViewModel() {

    private var _viewState : MutableLiveData<Resource<List<Bird>>> = MutableLiveData(Resource.Loading())
    val viewState: LiveData<Resource<List<Bird>>    > get() = _viewState

    init {
        viewModelScope.launch(Dispatchers.Main) {
            _viewState.value = repo.getBirds(31)
        }
    }


}