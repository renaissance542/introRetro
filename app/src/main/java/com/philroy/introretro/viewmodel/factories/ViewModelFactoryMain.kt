package com.philroy.introretro.viewmodel.factories

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.philroy.introretro.model.MainRepo
import com.philroy.introretro.viewmodel.MainViewModel

class ViewModelFactoryMain(
    private val repo: MainRepo
): ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MainViewModel(repo) as T
    }
}