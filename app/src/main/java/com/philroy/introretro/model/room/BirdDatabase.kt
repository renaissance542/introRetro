package com.philroy.introretro.model.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.philroy.introretro.model.Bird

@Database(entities = [Bird::class], exportSchema = false, version = 2)
abstract class BirdDatabase(): RoomDatabase() {
    abstract fun birdDao(): BirdDao

    companion object {
        private val DB_NAME = "Birds Database"
        fun getDatabaseInstance(context: Context): BirdDatabase {
            return Room.databaseBuilder(context, BirdDatabase::class.java, DB_NAME)
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}