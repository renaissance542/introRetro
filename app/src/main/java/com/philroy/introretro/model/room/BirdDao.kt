package com.philroy.introretro.model.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.philroy.introretro.model.Bird

@Dao
interface BirdDao {

    @Query("SELECT * FROM birds")
    suspend fun getBirds(): List<Bird>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addBirds(birds: List<Bird>)

}