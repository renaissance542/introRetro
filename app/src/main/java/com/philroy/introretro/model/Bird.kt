package com.philroy.introretro.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(tableName = "birds")
@Parcelize
data class Bird (
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val url:String
): Parcelable