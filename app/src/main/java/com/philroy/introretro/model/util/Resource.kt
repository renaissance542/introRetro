package com.philroy.introretro.model.util

import java.lang.NullPointerException

sealed class Resource<T>(data: T? = null, errorMsg: String? = null) {
    class Loading<T>(data: T? = null): Resource<T>(data)
    data class Success<T>(val data: T): Resource<T>(data)
    data class Error<T>(val errorMsg: String): Resource<T>(null, errorMsg)
}
