package com.philroy.introretro.model

import android.content.Context
import com.philroy.introretro.model.remote.ApiService
import com.philroy.introretro.model.room.BirdDatabase
import com.philroy.introretro.model.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MainRepo (val context: Context) {

    private val apiService by lazy { ApiService.retrofitInstance }
    private val birdDao = BirdDatabase.getDatabaseInstance(context).birdDao()


    suspend fun getBirds(count: Int): Resource<List<Bird>> = withContext(Dispatchers.IO) {
        return@withContext try {
            val response = apiService.getBirds(count)
            val dbInfo = birdDao.getBirds()
            if (response.isSuccessful && (response.body()!!.size > dbInfo.size)){
                val birds = response.body()!!.map { Bird(url = it) }
                birdDao.addBirds(birds)
                Resource.Success(birds)
            } else {
                Resource.Success(dbInfo)
            }
        } catch (e: Exception) {
            Resource.Error(e.localizedMessage?: "no localized error message")
        }
    }
}